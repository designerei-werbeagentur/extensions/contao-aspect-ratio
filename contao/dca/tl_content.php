<?php

declare(strict_types=1);

use designerei\ContaoAspectRatioBundle\EventListener\AspectRatioOptionsListener;
use Contao\CoreBundle\DataContainer\PaletteManipulator;

$GLOBALS['TL_DCA']['tl_content']['fields']['aspectRatio'] = [
  'exclude'                 => true,
  'inputType'               => 'select',
  'options_callback'        => [AspectRatioOptionsListener::class, 'onOptionsCallback'],
  'eval'                    => array('tl_class' => 'w50', 'multiple' => true, 'size' => '10', 'chosen' => true, 'mandatory' => false),
  'sql'                     => "text NULL"
];
