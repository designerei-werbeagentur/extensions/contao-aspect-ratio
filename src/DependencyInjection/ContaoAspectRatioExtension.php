<?php

declare(strict_types=1);

namespace designerei\ContaoAspectRatioBundle\DependencyInjection;

use designerei\ContaoAspectRatioBundle\AspectRatioClasses;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class ContaoAspectRatioExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../../config'));
        $loader->load('services.yml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $definition = $container->getDefinition(AspectRatioClasses::class);
        $definition->setArguments(array_values($config));
    }
}
