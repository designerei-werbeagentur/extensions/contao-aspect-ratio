<?php

declare(strict_types=1);

namespace designerei\ContaoAspectRatioBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('contao_aspect_ratio');

        // Keep compatibility with symfony/config < 4.2
        if (method_exists($treeBuilder, 'getRootNode')) {
           $rootNode = $treeBuilder->getRootNode();
        } else {
           $rootNode = $treeBuilder->root('contao_aspect_ratio');
        }

        $rootNode
            ->children()
                ->arrayNode('aspect_ratios')
                    ->scalarPrototype()->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
