<?php

declare(strict_types=1);

namespace designerei\ContaoAspectRatioBundle\EventListener;

use designerei\ContaoAspectRatioBundle\AspectRatioClasses;

final class AspectRatioOptionsListener
{
    private AspectRatioClasses $aspectRatioClasses;

    public function __construct(AspectRatioClasses $aspectRatioClasses)
    {
        $this->aspectRatioClasses = $aspectRatioClasses;
    }

    public function onOptionsCallback(): array
    {
        return $this->aspectRatioClasses->getAspectRatioOptions();
    }
}
