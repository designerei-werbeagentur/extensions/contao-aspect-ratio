<?php

declare(strict_types=1);

namespace designerei\ContaoAspectRatioBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use designerei\ContaoAspectRatioBundle\AspectRatioClasses;

/**
 * @Hook("initializeSystem")
 */
class AspectRatioSafelistListener
{
    private AspectRatioClasses $aspectRatioClasses;

    public function __construct(AspectRatioClasses $aspectRatioClasses)
    {
        $this->aspectRatioClasses = $aspectRatioClasses;
    }

    public function __invoke(): void
    {
        $this->aspectRatioClasses->createAspectRatioSafelist();
    }
}
