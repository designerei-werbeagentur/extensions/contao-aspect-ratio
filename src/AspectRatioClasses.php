<?php

declare(strict_types=1);

namespace designerei\ContaoAspectRatioBundle;

use Symfony\Component\Filesystem\Filesystem;

final class AspectRatioClasses
{
    private $aspectRatios;

    public function __construct($aspectRatios)
    {
        $this->aspectRatios = $aspectRatios;
        $this->viewports = array('sm','md','lg','xl');
        $this->prefix = 'aspect-';
    }

    public function getAspectRatioOptions(): array
    {
        $options = [];
        $aspectRatios = $this->aspectRatios;
        $viewports = $this->viewports;
        $prefix = $this->prefix;

        // convert colon to slash & and add square bracktes
        // https://tailwindcss.com/docs/aspect-ratio#arbitrary-values
        foreach ($aspectRatios as &$aspectRatio) {
           $aspectRatio = str_replace(':','/',$aspectRatio);
           $aspectRatio = "[" . $aspectRatio . "]";
        }
        unset($aspectRatio);

        // add option none
        if ($aspectRatios) {
            array_push($aspectRatios,'auto');
        }

        // viewports
        foreach ($aspectRatios as $aspectRatio) {
            $options[''][] = implode([$prefix . $aspectRatio]);
        }

        foreach ($viewports as $viewport) {
            foreach ($aspectRatios as $aspectRatio) {
                $options[$viewport][] = implode([$viewport . ':' . $prefix . $aspectRatio]);
            }
        }

        return $options;
    }

    public function createAspectRatioSafelist(): string
    {
        $safelist = '';
        $aspectRatios = $this->aspectRatios;
        $viewports = $this->viewports;
        $prefix = $this->prefix;

        // convert colon to slash & and add square bracktes
        // https://tailwindcss.com/docs/aspect-ratio#arbitrary-values
        foreach ($aspectRatios as &$aspectRatio) {
            $aspectRatio = str_replace(':','/',$aspectRatio);
            $aspectRatio = "[" . $aspectRatio . "]";
        }
        unset($aspectRatio);

        // add option none
        if ($aspectRatios) {
            array_push($aspectRatios,'auto');
        }

        foreach ($aspectRatios as $aspectRatio) {
            $safelist .= ' ' . $prefix . $aspectRatio;
        }

        foreach ($viewports as $viewport) {
            foreach ($aspectRatios as $aspectRatio) {
                $safelist .= ' ' . $viewport . ':' . $prefix . $aspectRatio;
            }
        }

        $safelist = ltrim($safelist);

        // create safelist file
        $cur_dir = \dirname(__DIR__);
        $public_dir = $cur_dir.'/src/Resources/public';

        $fs = new Filesystem();
        $fs->mkdir($public_dir);
        $fs->dumpFile($public_dir.'/safelist.txt', $safelist);

        return $safelist;
    }
}
