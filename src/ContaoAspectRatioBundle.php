<?php

namespace designerei\ContaoAspectRatioBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContaoAspectRatioBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}
